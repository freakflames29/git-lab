#include "stdio.h"
#include "stdlib.h"
struct node
 {
   int data;
   struct node* next;
  /* data */
};
void traverse(struct node* ptr)
{
  while (ptr!=NULL)
   {
     printf("%d\n",ptr->data );
     ptr=ptr->next;
    /* code */
  }
}
int main()
{
  struct node* first=(struct node*)malloc(sizeof(struct node));
  struct node* second=(struct node*)malloc(sizeof(struct node));

  struct node* third=(struct node*)malloc(sizeof(struct node));
  first->data=10;
  first->next=second;

  second->data=20;
  second->next=third;

  third->data=30;
  third->next=NULL;
  printf("The linked list\n" );
  traverse(first);

}
