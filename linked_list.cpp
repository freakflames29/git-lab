#include "iostream"
using namespace std;
struct node
{
  int data;
  node* next;
};
void traverse(node* ptr)
{
  while (ptr!=NULL)
   {
     cout<<ptr->data<<" ";
     ptr=ptr->next;
    /* code */
  }
}

int main()
{
  node* first=new  node();
  node* second=new  node();
  node* third=new  node();

  first->data=50;
  first->next=second;

  second->data=60;
  second->next=third;

  third->data=70;
  third->next=NULL;
  cout<<"the linked list"<<endl;
  traverse(first);

}
